#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name = "django-flexible-form",
    version = "1.0.0",
    url = 'http://ondrejsika.com/docs/django-flexible-form',
    download_url = 'https://github.com/sikaondrej/django-flexible-form',
    license = 'GNU LGPL v.3',
    description = "",
    author = 'Ondrej Sika',
    author_email = 'ondrej@ondrejsika.com',
    py_modules = ["flexible_form"],
    #packages = find_packages(),
)
